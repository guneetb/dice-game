import { Component, OnInit } from '@angular/core';
import { DiceService } from '../dice.service';


@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  submissionData = {};
  winplayer: string="All the best!!";
  isShow=true;
  constructor(private _DiceServie: DiceService) {

  }

  ngOnInit() {
    this._DiceServie._diceResultData$.subscribe(save => {
      this.submissionData = save;

      this.isShow = false;
      if(save==" "){
        this.isShow = true;
        this.winplayer = "All the best!!";
      }
    }
    );

  }

  submission():void {
    
   // calculateResult(JSON.stringify(this.submissionData));
   
    // function calculateResult(input: string) {

      var scores = [];
      var resultStrings = [];
      var cardsData = JSON.parse((JSON.stringify(this.submissionData)));

      for (var prop in cardsData) {
        encode(cardsData[prop]);
        var score = checkHand(cardsData[prop]);
        //  console.log("score" + score);
        scores.push(score);
      }
      //  console.log("scores" + scores);
      var maxScore = Math.max(...scores);
      var found = scores.findIndex(function (element) {
        return element == maxScore;
      });
      var winners = [];
      for (var i = found + 1; i < scores.length; i++) {
        if (scores[i] == scores[found]) {
          winners.push(i);
        }
      }
      found++;
      if (winners.length > 0) {
        var winList = "";
        for (i = 0; i < winners.length; i++) {
          var winNumber = winners[i] + 1;
          winList = winList + winNumber + ", ";
        }

        console.log("Tie between players " + found + ", " + winList + " with " + resultStrings[found - 1]);
        this.winplayer =("Tie between players " + found + ", " + winList + " with " + resultStrings[found - 1]);

      } else {
        console.log("Player " + found + " wins with " + resultStrings[found - 1]);
       this.winplayer=("Player " + found + " wins with " + resultStrings[found - 1]);
        // console.log(this.winplayer );
      }
    
      function checkHand(hand: string) {
        var resultString = "";
        var score = 0;
        switch (duplicateCards(hand)) {
          case "2":
            resultString = "1 Pair";
            score = 1;
            break;
          case "22":
            resultString = "2 Pairs";
            score = 2;
            break;
          case "3":
            resultString = "3 of a Kind";
            score = 3;
            break;
          case "23":
          case "32":
            resultString = "Full House";
            score = 5;
            break;
          case "4":
            resultString = "4 of a Kind";
            score = 6;
            break;
          case "5":
            resultString = "5 of a Kind";
            score = 7;
            break;
          default:
            if (isStraight(hand)) {
              resultString = "Straight";
              score = 4;
              if (occurrencesOf(hand, 14) == 1) {
                resultString = "Ace Straight";
              }
            }
            break;
        }
        if (!resultString) {
          score = 0;
          resultString = "nothing...";
        }
        resultStrings.push(resultString);
        //console.log(resultString+','+score);

        return score;
      }
      function encode(c: any) {
        c.forEach(function (item: string, i: number) {
          if (item == '9') c[i] = 9;
          if (item == '10') c[i] = 10;
          if (item == 'J' || item == 'j') c[i] = 11;
          if (item == 'Q' || item == 'q') c[i] = 12;
          if (item == 'K' || item == 'k') c[i] = 13;
          if (item == 'A' || item == 'a') c[i] = 14;
        });
        //console.log(c);
      }

      function duplicateCards(valuesArray) {
        var occurrencesFound = [];
        var result = "";
        for (var i = 0; i < valuesArray.length; i++) {
          var occurrences = occurrencesOf(valuesArray, valuesArray[i]);
          if (occurrences > 1 && occurrencesFound.indexOf(valuesArray[i]) == -1) {
            result += occurrences;
            occurrencesFound.push(valuesArray[i]);
          }
        }
        return result;
      }
      function occurrencesOf(valuesArray, n) {
        var count = 0;
        var index = 0;
        do {
          index = valuesArray.indexOf(n, index) + 1;
          if (index == 0) {
            break;
          }
          else {
            count++;
          }
        } while (index < valuesArray.length);
        return count;
      }
      function isStraight(valuesArray) {
        var lowest = getLowest(valuesArray);
        for (var i = 1; i < 5; i++) {
          if (occurrencesOf(valuesArray, lowest + i) != 1) {
            return false
          }
        }
        return true;
      }

      function getLowest(valuesArray) {
        var min = 14;
        for (var i = 0; i < valuesArray.length; i++) {
          min = Math.min(min, valuesArray[i]);
        }
        return min;
      }

    }

  //}
  
}